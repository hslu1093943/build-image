# Build Image

## build the image

This repository builds a nginx image based on 'Dockerfile' and pushes it to the Gitlab Container Registry.
Check out .gitlab-ci.yml for the pipeline.

See "Deploy" - "Container Registry" after the pipeline run.

## run the image

Create new workspace on Gitpod.io using this repository.

Login to registry, pull the image and analyze it.
Checkout ${DOCKER_ENV_CI_REGISTRY_IMAGE} for details.

```bash
docker login registry.gitlab.com

docker pull registry.gitlab.com/<yourGitlab>/build-image/nginx:1.0

docker history --human --no-trunc registry.gitlab.com/<yourGitlab>/build-image/nginx:1.0

docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive:latest registry.gitlab.com/<yourGitlab>/build-image/nginx:1.0
```

Start new container using the nginx image. Start a shell instead of the nginx

```bash
docker run --name nginx --rm -it --entrypoint /bin/sh registry.gitlab.com/<yourGitlab>/build-image/nginx:1.0

id

ps
```

Start new container using the nginx image. Detach and let it run as a daemon. 
Gitpod.io will ask you to open port in new browser tap.
Inspect container, which are the differences to inspect image?

```bash
docker run --name nginx --rm -d -p 8080:8080 registry.gitlab.com/<yourGitlab>/build-image/nginx:1.0

docker container inspect nginx
```

Connect to running container and execute shell as user nginx.

```bash
docker exec -it --user nginx nginx /bin/sh

id 

ps
```

## housekeeping

Check containers and remove them if necessary.

```bash
docker ps --all

docker rm --force <Containers>
```

Check images and remove them if necessary.

```bash
docker images

docker rmi <Images>
```