FROM alpine:latest

RUN apk update \
 && apk add --update openssl nginx-debug curl \
 && ln -sf /dev/stdout /var/log/nginx/access.log \ 	
 && ln -sf /dev/stderr /var/log/nginx/error.log \
 && touch /var/run/nginx/nginx.pid \
 && chown -R nginx:nginx /var/run/nginx/nginx.pid /run/nginx/nginx.pid \
 && mkdir /html \
 && chown -R nginx:nginx /html \
 && rm -rf /var/cache/apk/*

COPY etc/*.conf /etc/nginx/
COPY html/* /html/

HEALTHCHECK --interval=1m --timeout=3s \
CMD curl --fail http://localhost:8080/ || exit 1

EXPOSE 8080

USER nginx

ENTRYPOINT ["/usr/sbin/nginx"]
